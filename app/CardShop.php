<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardShop extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $table = 'card_shops';
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
