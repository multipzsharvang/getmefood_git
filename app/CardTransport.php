<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardTransport extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $table = 'card_transports';
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
