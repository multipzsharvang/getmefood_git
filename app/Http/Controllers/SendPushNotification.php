<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Transporter;
use Exception;
use Log;
use Setting;
use App\Notification;
use PushNotification;
use App\Shop;

/*use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;
use App\Notifications\WebPush;*/
class SendPushNotification extends Controller
{
	

    /**
     * Money added to user wallet.
     *
     * @return void
     */
    public function WalletMoney($user_id, $money){

        Log::info($user_id);

        return $this->sendPushToUser($user_id, $money.' '.trans('form.push.added_money_to_wallet'));
    }

    /**
     * Money charged from user wallet.
     *
     * @return void
     */
    public function ChargedWalletMoney($user_id, $money){

        return $this->sendPushToUser($user_id, $money.' '.trans('form.push.charged_from_wallet'));
    }

    /**
     * Sending Push to a user Device.
     *
     * @return void
     */
    public function sendPushToUser($user_id, $push_message,$page = null){

    	try{

	    	$user = User::findOrFail($user_id);
            
            Notification::create([
                'user_id' => $user_id,
                'message' => $push_message
            ]);
            // $message = \PushNotification::Message($push_message,array(
            //     'badge' => 1,
            //     'sound' => 'example.aiff',
                
            //     'actionLocKey' => 'Action button title!',
            //     'locKey' => 'localized key',
            //     'locArgs' => array(
            //         'localized args',
            //         'localized args',
            //     ),
            //     'launchImage' => 'image.jpg',
                
            //     'custom' => array('custom data' => array(
            //         $page
            //     ))
            // ));
            if($user->device_token != ""){

                \Log::info($page);
                $device_token = $user->device_token;
    	    	if($user->device_type == 'ios'){

    	    	// return \PushNotification::app('IOSUser')
    		      //        ->to($user->device_token)
    		     //        ->send($message);
                return $this->sendPushNotificationUsingFCM($push_message,$device_token,$page = null);

    	    	}elseif($user->device_type == 'android'){
    	    		return $this->sendPushNotificationUsingFCM($push_message,$device_token,$page = null);
                }
            }

    	} catch(Exception $e){
    		return $e;
    	}

    }
    

     /**
     * Sending Push to a user Device.
     *
     * @return void
     */
    public function sendPushToShop($user_id, $push_message,$page = null){

        try{

            $user = Shop::findOrFail($user_id);
                Notification::create([
                    'user_id' => $user_id,
                    'message' => $push_message
                ]);
            // $message = PushNotification::Message($push_message,array(
            //     'badge' => 1,
            //     'sound' => 'example.aiff',
                
            //     'actionLocKey' => 'Action button title!',
            //     'locKey' => 'localized key',
            //     'locArgs' => array(
            //         'localized args',
            //         'localized args',
            //     ),
            //     'launchImage' => 'image.jpg',
                
            //     'custom' => array('custom data' => array(
            //         $page
            //     ))
            // ));
            if($user->device_token != ""){

                \Log::info('sending push for user : '. $user->name);
                 $device_token = $user->device_token;
                if($user->device_type == 'ios'){

                    // return \PushNotification::app('IOSUser')
                 //        ->to($user->device_token)
                 //        ->send($message);
                    return $this->sendPushNotificationUsingFCM($push_message,$device_token,$page = null);

                }elseif($user->device_type == 'android'){
                    return $this->sendPushNotificationUsingFCM($push_message,$device_token,$page = null);
                }
            }

        } catch(Exception $e){
            return $e;
        }

    }

    /**
     * Sending Push to a user Device.
     *
     * @return void
     */
    public function sendPushToProvider($provider_id, $push_message,$page){

    	try{

	    	$provider = Transporter::findOrFail($provider_id);
                Notification::create([
                    'transporter_id' => $provider_id,
                    'message' => $push_message
                ]);

            // $message = PushNotification::Message($push_message,array(
            //     'badge' => 1,
            //     'sound' => 'example.aiff',
                
            //     'actionLocKey' => 'Action button title!',
            //     'locKey' => 'localized key',
            //     'locArgs' => array(
            //         'localized args',
            //         'localized args',
            //     ),
            //     'launchImage' => 'image.jpg',
                
            //     'custom' => array('custom data' => array(
            //         $page
            //     ))
            // ));
            if($provider->device_token != ""){
                
                \Log::info('sending push for provider : '. $provider->name);

            	$device_token = $provider->device_token;
                if($provider->device_type == 'ios'){

                    // return \PushNotification::app('IOSUser')
                 //        ->to($user->device_token)
                 //        ->send($message);
                    return $this->sendPushNotificationUsingFCM($push_message,$device_token,$page = null);

                }elseif($provider->device_type == 'android'){
                    return $this->sendPushNotificationUsingFCM($push_message,$device_token,$page = null);
                }
            }

    	} catch(Exception $e){
    		return $e;
    	}

    }
    function sendPushNotificationUsingFCM($message,$device_token,$page = null){
        $api_key = env('ANDROID_PUSH_KEY');
        $title = "GETMEFOOD";
        $fields = [
            'to' => $device_token,
            'notification' => [
                    'body' =>$message,
                    'title'=>$title,
                    'badge' =>1,
                    'sound' => 'example.aiff',
                    'actionLocKey' => 'Action button title!',
                    'locKey' => 'localized key',
                    'locArgs' => array(
                        'localized args',
                        'localized args',
                    ),
                    'launchImage' => 'image.jpg',
                    
                    'custom' => array('custom data' => array(
                        $page
                    ))
            ],
            "priority"=> "high", 
            'data'=>[
                    'body' =>$message,
                    'title'=>$title,
            ]
        ];
        $fields = json_encode($fields);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $fields,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: key=' . $api_key
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

}
