<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WalletShopPassbook extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $table = 'wallet_shop_passbook';
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];
    public function username()
    {
        return $this->hasOne('App\Shop','id','user_id');
    }
}
