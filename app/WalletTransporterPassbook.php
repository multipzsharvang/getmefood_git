<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WalletTransporterPassbook extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
     protected $table = 'wallet_transporter_passbook';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];
    public function username()
    {
        return $this->hasOne('App\Transporter','id','transporter_id');
    }
}
