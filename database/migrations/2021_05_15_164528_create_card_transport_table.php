<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardTransportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_transports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transporter_id');
            $table->enum('card_type',['stripe','braintree'])->default('stripe');;
            $table->string('last_four');
            $table->string('card_id');
            $table->string('brand')->nullable();
            $table->integer('is_default')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
