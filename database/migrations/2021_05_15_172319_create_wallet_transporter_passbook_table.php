<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletTransporterPassbookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet_transporter_passbook', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transporter_id');
            $table->integer('order_id')->nullable();
            $table->string('amount')->nullable();
            $table->string('message')->nullable();
            $table->enum('status', [
                    'CREDITED',             
                    'DEBITED',
                    'PROCESSING'
                ])->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
