@extends('admin.layouts.app')

@section('content')

<!-- File export table -->
<div class="row file">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">@lang('payout.payout.delivery_title')</h4>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard table-responsive">
                    <table class="table table-striped table-bordered file-export">
                        <thead>
                            <tr>
                                <th>@lang('payout.payout.sl_no')</th>
                                <th>@lang('payout.order_id')</th>
                                <th>@lang('payout.username')</th>
                                <th>@lang('payout.mobile')</th>
                                <!-- <th>@lang('payout.stripe_card')</th> -->
                                <th>@lang('payout.amount')</th>
                                <th>@lang('payout.status')</th>
                                <th>@lang('inventory.cuisine.action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($deliveryPayout as $Index => $data)
                                <tr>
                                    <td>{{ $Index+1 }}</td>
                                    <td>{{ $data->order_id }}</td>
                                    <td>{{ $data->username->name }}</td>
                                    <td>{{ $data->username->country_code.''.$data->username->phone }}</td>
                                   <!--  <td>{{ $data->user_id }}</td> -->
                                    <td>{{ $data->amount }}</td>
                                    <td><span class="badge badge-info">{{ $data->status }}</span></td>
                                    <td>
                                        @if(Setting::get('DEMO_MODE')==1 && $data->status =="PROCESSING")
                                       <!--  <a href="{{ route('admin.cuisines.edit', $data->id) }}" class="table-btn btn btn-icon btn-success"><i class="fa fa-pencil-square-o"></i></a> -->
                                        
                                        <button onclick="return confirm('Are you sure to pay money??');"  class="table-btn btn btn-icon btn-danger" form="resource-update-{{ $data->id }}" >Pay Now</button> 
                                        <form id="resource-update-{{ $data->id }}" action="{{ route('admin.delivery.payout.update', $data->id)}}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="payout_id" value="{{$data->id}}">
                                        </form>
                                        @else
                                            -
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9999" class="text-center">
                                        <h4>@lang('payout.payout.no_record_found')</h4>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


