@extends('shop.layouts.app')

@section('content')
  <div class="card">
    <div class="card-header">
        <h4 class="card-title">Deliveries</h4>
        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
        <div class="heading-elements">
            <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="card-body collapse in">
        <div class="card-block">
            <form>
            <input type="hidden" name="list" value="true" />
            <input type="hidden" name="all" value="{{Request::get('all')}}" />
            <div class="form-group col-xs-12 mb-2">
                
                    <label>Delivery People</label>
                    <select id="user_name" name="dp" class="form-control">
                         <option value=""   >Select</option>
                        @forelse($Providers as $id=>$name)
                            <option value="{{$id}}"  @if(Request::get('dp')==$id) selected  @endif  >{{$name}}</option>
                        @empty
                        @endforelse
                    </select>
                
            </div>
            <div class="form-group col-xs-12 ">
                <div class="form-group col-xs-6 ">
                    <label>Start Date</label>
                    <input type="text" id="date" required  readonly name="start_date" value="{{Request::get('start_date')}}" class="form-control datepicker"/>
                </div>
                <div class="form-group col-xs-6 ">
                    <label>End Date</label>
                    <input type="text" id="date" required  readonly name="end_date" value="{{Request::get('end_date')}}" class="form-control datepicker"/>
                </div>
            </div>
            <button class="pull-right btn btn-success">Search</button>
            </form>
        </div>
        <?php $total_earning = $total_gross = $total_delivery = $total_food_commision = 0; ?>
        <div class="card-block card-dashboard table-responsive">
            <table class="table table-striped table-bordered file-export">
                <thead>
                    <tr>
                        <th>S.no</th>
                        <th>Customer Name</th>
                        <th>Delivery People</th>
                        <th>Restaurant</th>
                        <th>Address</th>
                        <th>Cost</th>
                        <th>Status</th>
                        <th>Order List</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($Orders as $Index => $Order)
                    <?php $total_earning +=$Order->invoice->net;
                          $total_gross +=$Order->invoice->gross;
                          $total_delivery +=$Order->invoice->delivery_charge;
                           $total_food_commision +=$Order->invoice->getfood_service_charge;
                     ?>
                    <tr>
                        <td>{{ $Index + 1 }}</td>
                        <td>{{ $Order->user->name }}</td>
                        <td>
                            {{ @$Order->transporter->name }}
                        </td>
                        <td>{{ @$Order->shop->name }}</td>
                        <td>{{ @$Order->address->building }}</td>
                        <td>{{Setting::get('currency')}}{{ @$Order->invoice->net }}</td>
                        <td><span class="tag tag-success">{{$Order->status}}</span></td>
                        <td>
                            <button class="btn btn-primary btn-darken-3 tab-order orderlist" data-id="{{$Order->id}}" >Order List</button>
                        </td>
                    </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
        </div>
        <div class="card-block" style="display: none;">
        <h3>Total Earning:- </h3>
        @if(count($Orders)>0)
        <?php 

            //$total_food_commision = $total_gross*(Setting::get('COMMISION_OVER_FOOD')/100);
            $total_delivery_commision = $total_delivery*(Setting::get('COMMISION_OVER_DELIVERY_FEE')/100);
        ?>
            <div class="row m-0">
                <dt class="col-sm-3 order-txt p-0">Total Earning</dt>
                <dd class="col-sm-9 order-txt "><span>: {{currencydecimal($total_earning)}}</span></dd>
            </div>
            <div class="row m-0">
                <dt class="col-sm-3 order-txt p-0">Commision from Food Items</dt>
                <dd class="col-sm-9 order-txt "><span>: {{currencydecimal($total_food_commision)}}</span> </dd>
            </div>
            <div class="row m-0">
                <dt class="col-sm-3 order-txt p-0">Commision from Delivery Charge</dt>
                <dd class="col-sm-9 order-txt "><span>: {{currencydecimal($total_delivery_commision)}}</span> </dd>
            </div>
            <div class="row m-0">
                <dt class="col-sm-3 order-txt p-0">Total Commision </dt>
                <dd class="col-sm-9 order-txt "><span>: {{currencydecimal($total_food_commision+$total_delivery_commision)}}</span> </dd>
            </div>
        @endif
        </div>
    </div>
</div>


<!-- Order List Modal Starts -->
<div class="modal fade text-xs-left" id="order-list">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="modal-title" id="myModalLabel1">Order List</h2>
                <!-- <div><p id="order_timer"></p></div> -->
            </div>
            <div class="modal-body">
                <div class="row m-0">
                    <dl class="order-modal-top">
                        <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Order ID</dt>
                            <dd class="col-sm-9 order-txt orderid"></dd>
                        </div>
                        <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Restaurant Name</dt>
                            <dd class="col-sm-9 order-txt rest_name"><span>: </span> Burger King</dd>
                        </div>
                        <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Customer Name</dt>
                            <dd class="col-sm-9 order-txt cust_name"><span>: </span> William Hawings</dd>
                        </div>
                        <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Address</dt>
                            <dd class="col-sm-9 order-txt address">
                                <span>: </span> 20B, Northeasrt Street,
                                <br> Newuork City,
                                <br> United States.
                            </dd>
                        </div>
                        <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Phone Number</dt>
                            <dd class="col-sm-9 order-txt cust_phone"><span>: </span> +12 445 8878 989</dd>
                        </div>
                         <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Delivery Date</dt>
                            <dd class="col-sm-9 order-txt cust_delivery_date"></dd>
                        </div>
                         <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Note</dt>
                            <dd class="col-sm-9 order-txt cust_order_note"></dd>
                        </div>
                        <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Total Amount</dt>
                            <dd class="col-sm-9 order-txt tot_amt"><span>: </span> $1600</dd>
                            <br/>
                            <br/>
                        </div>
                        <div class="row m-0" >
                            <dt class="col-sm-3 order-txt p-0">Status</dt>
                            <dt class="col-sm-9 order-txt ">Time</dt>
                        </div>
                         <div class="row m-0" id="order_status_list">
                            <dt class="col-sm-3 order-txt p-0">INCOMING</dt>
                            <dd class="col-sm-9 order-txt ">  {{date("Y-m-d H:i:s")}}</dd>
                        </div>
                    </dl>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Product Image</th>
                                    <th>Product Name</th>
                                    <th>Note</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Cost</th>
                                </tr>
                            </thead>
                            <tbody class="cartstbl">
                                
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>@lang('user.create.discount')</th>
                                    <td class="text-right discount">$0</td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>@lang('user.create.servicetax') ({{Setting::get('tax')}}%)</th>
                                    <td class="text-right tax">$0</td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>@lang('user.create.delivery_charges')</th>
                                    <td class="text-right deliverycharge">$0</td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>@lang('user.create.tip')</th>
                                    <td class="text-right tip">$0</td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>@lang('user.create.promocode_discount')</th>
                                    <td class="text-right promocode_amount">$0</td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>@lang('user.create.wallet_amount')</th>
                                    <td class="text-right wallet_amount">$0</td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>@lang('user.create.user_pay')</th>
                                    <td class="text-right tot_amt">$0</td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>@lang('user.create.to_pay')</th>
                                    <td class="text-right to_pay">$0</td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>@lang('user.Delivery_fee')</th>
                                    <td class="text-right delivery_fee">$0</td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>Stripe fee({{Setting::get('stripe_charge')}}%)</th>
                                    <td class="text-right stripe_charge">$0</td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>@lang('user.create.servicetax') ({{Setting::get('tax')}}%)</th>
                                    <td class="text-right service_tax">$0</td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>@lang('user.create.service_getme_food_charge')</th>
                                    <td class="text-right service_getme_food_charge">$0</td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>@lang('user.create.resturant_get_amount')</th>
                                    <td class="text-right resturant_get_amount">$0</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Order List Modal Ends -->
@endsection
@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/timeTo.css')}}">
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('assets/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
 <script src="{{ asset('assets/js/jquery.time-to.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$('.datepicker').datepicker();
$('.orderlist').on('click',function(){ 
    var order_id = $(this).data('id');
    var order_date;
    $.ajax({
        url: "{{url('shop/orders')}}/"+order_id,
        success: function(result){
            $('#order_timer').timeTo({
                timeTo: order_date,
                theme: "black",
                displayCaptions: true,
                fontSize: 30,
                captionSize: 10
            });
            order_date = new Date(new Date(result.Order.created_at));
            $('.orderid').html('<span>: </span>'+result.Order.id);
            $('.rest_name').html('<span>: </span>'+result.Order.shop.name);
            $('.cust_name').html('<span>: </span>'+result.Order.user.name);
            @if(Setting::get('demo_mode')==0)
            $('.cust_phone').html('<span>: </span>'+(result.Order.user.phone).substring(0, 3)+'xxxxxxxxx');
            @else
            $('.cust_phone').html('<span>: </span>'+result.Order.user.phone);
            @endif
            if(result.Order.delivery_date){
                $('.cust_delivery_date').html('<span>: </span>'+result.Order.delivery_date);
            }else{
                $('.cust_delivery_date').html('<span>: </span>'+result.Order.created_at);
            }
            if(result.Order.note){
                $('.cust_order_note').html('<span>: </span>'+result.Order.note);
            }else{
               $('.cust_order_note').html('<span>: -- </span>'); 
            }
            $('.address').html('<span>: </span>'+result.Order.address.map_address);
            $('.tot_amt').html('{{Setting::get("currency")}}'+result.Order.invoice.payable.toFixed(2));
            var delivery_charge = 0.00;
            if(result.Order.invoice.delivery_charge != null || result.Order.invoice.delivery_charge != undefined){
                delivery_charge = result.Order.invoice.delivery_charge;
            }

            $('.deliverycharge').html("{{Setting::get('currency')}}"+parseFloat(delivery_charge).toFixed(2));
            //$('.delivery_fee').html("-{{Setting::get('currency')}}"+parseFloat(delivery_charge).toFixed(2));


            //$('.tax').html("{{Setting::get('currency')}}"+result.Order.invoice.tax.toFixed(2));
            //$('.stripe_charge').html("<span>: </span> {{Setting::get('currency')}}"+result.Order.invoice.tax.toFixed(2));  
            var tax= 0.00;
            if(result.Order.invoice.tax != null || result.Order.invoice.tax != undefined){
                tax = result.Order.invoice.tax;
            }

            $('.tax').html("{{Setting::get('currency')}}"+parseFloat(tax).toFixed(2));
            $('.service_tax').html("-{{Setting::get('currency')}}"+parseFloat(tax).toFixed(2));

            var discount=0.00;
            if(result.Order.invoice.discount != null || result.Order.invoice.discount != undefined){
                discount = result.Order.invoice.discount;
            }
            $('.discount').html("-{{Setting::get('currency')}}"+parseFloat(discount).toFixed(2)); 
            
            var tip=0.00;
            if(result.Order.invoice.tip != null || result.Order.invoice.tip != undefined){
                tip = result.Order.invoice.tip;
            }
            $('.tip').html("{{Setting::get('currency')}}"+parseFloat(tip).toFixed(2));

            var delivery_boy_payble_amount = parseFloat(delivery_charge)+parseFloat(tip);
            $('.delivery_fee').html("-{{Setting::get('currency')}}"+parseFloat(delivery_boy_payble_amount).toFixed(2));

            var wallet_amount=0.00;
            if(result.Order.invoice.wallet_amount != null || result.Order.invoice.wallet_amount != undefined){
                wallet_amount = result.Order.invoice.wallet_amount;
            }
            $('.wallet_amount').html("-{{Setting::get('currency')}}"+parseFloat(wallet_amount).toFixed(2));

            var promocode_amount=0.00;
            if(result.Order.invoice.promocode_amount != null || result.Order.invoice.promocode_amount != undefined){
                promocode_amount = result.Order.invoice.promocode_amount;
            }
            $('.promocode_amount').html("-{{Setting::get('currency')}}"+parseFloat(promocode_amount).toFixed(2));

            var stripe_charge=0.00;
            if(result.Order.invoice.stripe_charge != null || result.Order.invoice.stripe_charge != undefined){
                stripe_charge = result.Order.invoice.stripe_charge;
            }
            $('.stripe_charge').html("-{{Setting::get('currency')}}"+parseFloat(stripe_charge).toFixed(2));

            var service_getme_food_charge=0.00;
            if(result.Order.invoice.getfood_service_charge != null || result.Order.invoice.getfood_service_charge != undefined){
                service_getme_food_charge = result.Order.invoice.getfood_service_charge;
            }
            $('.service_getme_food_charge').html("-{{Setting::get('currency')}}"+parseFloat(service_getme_food_charge).toFixed(2));

            var resturant_get_amount=0.00;
            if(result.Order.invoice.payable != null || result.Order.invoice.payable != undefined){
                resturant_get_amount = parseFloat(result.Order.invoice.net) - parseFloat(delivery_charge) - parseFloat(tip) - parseFloat(stripe_charge) - parseFloat(service_getme_food_charge) - parseFloat(tax);
            }
            $('.resturant_get_amount').html("{{Setting::get('currency')}}"+parseFloat(resturant_get_amount).toFixed(2));
            
            $('.to_pay').html("{{Setting::get('currency')}}"+parseFloat(result.Order.invoice.net).toFixed(2));

            $('#order-list').modal('show');
            var statuslist='';
            $.each( result.Order.ordertiming, function( key, value ) {
                statuslist+='<dd class="col-sm-3 order-txt p-0">'+value.status+'</dd>\
                <dd class="col-sm-9 order-txt "> '+value.created_at+'</dd>';
            });
            $('#order_status_list').html(statuslist);
            var table = '';
            //console.log(result.Cart);
            $.each( result.Cart, function( key, value ) {
            table +='<tr>';
               if(value.product.images.length>0){
                table +='<td><div class="bg-img order-img" style="background-image: url('+value.product.images[0].url+');"></div></td>';
                }
                var note='-';
                if(value.note != null || value.note != undefined){
                    note = value.note;
                }

                table +='<td>'+value.product.name+'</td><td>'+note+'</td><td>{{Setting::get('currency')}}'+value.product.prices.price.toFixed(2)+'</td><td>'+value.quantity+'</td><td>{{Setting::get('currency')}}'+(value.quantity*value.product.prices.price).toFixed(2)+'</td></tr>';
                    $.each(value.cart_addons, function( key, valuee ) { console.log(valuee.quantity);
                            table +="<tr>\
                            <td></td><td></td><td class='text-left'>\
                                <h5>"+valuee.addon_product.addon.name+"</h5>\
                                <p>(Addons)</p>\
                            </td>\
                            <td>\
                                <strong>{{Setting::get('currency')}}"+valuee.addon_product.price.toFixed(2)+"</strong>\
                            </td>\
                            <td>"+value.quantity+"X"+valuee.quantity+"</td>\
                            <td>\
                                <p>{{Setting::get('currency')}}"+(value.quantity*valuee.addon_product.price*valuee.quantity).toFixed(2)+"</p>\
                            </td>\
                        </tr>";  
                     });
                
            });
            $('.cartstbl').html(table);
        }
    });
})
</script> 

@endsection

@section('styles')


@endsection