@extends('shop.layouts.app')

@section('content')

<!-- File export table -->
<div class="row file">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title col-md-4">@lang('payout.payout.title')</h4>
                <h4 class="card-title col-md-4" >
                    Your Card : xxxx xxxx xxxx {{$cardNumber??'-'}}   
                </h4>    
                <h4 class="card-title col-md-4" style="text-align: right;">Your Wallet : {{Setting::get('currency')}}{{$wallet_balance}}</h4>
                <div class="col-xs-12 main_payout_cashout" style="text-align: right;">
                    <div style="display:inline-flex;">
                        @if(empty(Auth::user()->stripe_cust_id))
                            <a href="{{ route('shop.card.create')}}" class="btn btn-primary" >Add Card</a>
                        @else
                            <button type="button" class="btn btn-icon btn-primary request_payout_modal" >Cashout Now</button>
                            <!-- <form action="{{ route('shop.request.cashout')}}" method="POST" style="margin-top: 10px;">
                                {{ csrf_field() }}
                                <input type="hidden" name="amount" value="{{$wallet_balance}}">
                                <button onclick="return confirm('Are you sure to cashout money??');"  class="btn btn-icon btn-primary" >Cashout Now</button>
                            </form> -->
                        @endif
                    </div>
                </div>    
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard table-responsive">
                    <table class="table table-striped table-bordered file-export">
                        <thead>
                            <tr>
                                <th>@lang('payout.payout.sl_no')</th>
                                <th>@lang('payout.order_id')</th>
                                <!-- <th>@lang('payout.username')</th>
                                <th>@lang('payout.mobile')</th> -->
                                <!-- <th>@lang('payout.stripe_card')</th> -->
                                <th>@lang('payout.amount')</th>
                                <th>@lang('payout.status')</th>
                                <!-- <th>@lang('inventory.cuisine.action')</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($passbook as $Index => $data)
                                <tr>
                                    <td>{{ $Index+1 }}</td>
                                    <td>{{ $data->order_id??'-' }}</td>
                                    <!-- <td>{{ $data->username->name }}</td> -->
                                   <!--  <td>{{ $data->username->country_code.''.$data->username->phone }}</td> -->
                                   <!--  <td>{{ $data->user_id }}</td> -->
                                    <td>{{ number_format($data->amount,2) }}</td>
                                    <td><span class="badge badge-info">{{ $data->status }}</span></td>
                                    <!-- <td>
                                        @if(Setting::get('DEMO_MODE')==1 && $data->status =="PROCESSING")
                                      
                                        <button onclick="return confirm('Are you sure to pay money??');"  class="table-btn btn btn-icon btn-danger" form="resource-update-{{ $data->id }}" >Pay Now</button> 
                                        <form id="resource-update-{{ $data->id }}" action="{{ route('admin.restaurant.payout.update', $data->id)}}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="payout_id" value="{{$data->id}}">
                                        </form>
                                        @else
                                            -
                                        @endif
                                    </td> -->
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9999" class="text-center">
                                        <h4>@lang('payout.payout.no_record_found')</h4>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="view_card_modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="ion-close-round"></span>
                </button>
                <h6>Add Tip</h6>
            </div>
            <div class="modal-body">
                <div class="">
                    <div class="col-xs-12">
                        <input type="number" name="add_more_tip" class="custom_add_more_tip form-control" placeholder="Enter Your Tip" style="height: 35px;margin: 25px 0px 10px 0px;">
                    </div>    
                </div>
            </div>
            <div class="modal-footer" style="padding-right:15px">
                <button type="button" class="tip_submit_btn btn btn-primary">Add Tip</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="request_payout" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="ion-close-round"></span>
                </button>
                <h6>Cashout Money</h6>
            </div>
            <form action="{{ route('shop.request.cashout')}}" method="POST" >
                 {{ csrf_field() }}
                <div class="modal-body" style="padding:0px;">
                    <div class="">
                        <div class="col-xs-12">
                            <input type="number" name="amount" class="form-control" placeholder="Enter Your amount" min="1" max="{{$wallet_balance}}" style="height: 35px;margin: 25px 0px 10px 0px;">
                        </div>    
                    </div>
                </div>
                <div class="modal-footer" style="padding-right:15px">
                    <button type="submit" class="tip_submit_btn btn btn-primary">Submit</button>
                </div>
            </form>    
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $('.view_card').click(function(){
        $('#view_card_modal').modal('show');
    });
    $('.request_payout_modal').click(function(){
        $('#request_payout').modal('show');
    });
</script>
@endsection


